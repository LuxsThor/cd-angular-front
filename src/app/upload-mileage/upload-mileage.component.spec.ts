import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadMileageComponent } from './upload-mileage.component';

describe('UploadMileageComponent', () => {
  let component: UploadMileageComponent;
  let fixture: ComponentFixture<UploadMileageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadMileageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadMileageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
