import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { Mileage } from './../mileage';
import { MatInputModule } from '@angular/material';
import { ConfimationComponent } from '../confimation/confimation.component';
import { UploadMileageService } from './../upload-mileage.service';
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";


@Component({
  selector: 'app-upload-mileage',
  templateUrl: './upload-mileage.component.html',
  styleUrls: ['./upload-mileage.component.scss']
})

export class UploadMileageComponent implements OnInit {
  mileage = new Mileage();
  submitted = false;
  node: number;

  constructor(private uploadMileageService: UploadMileageService, private dialog: MatDialog, private dialogRef: MatDialogRef<UploadMileageComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
   }

  ngOnInit() {
  }

  uploadMileage(){
    this.submitted = true;
    this.uploadMileageService.createUpload(this.mileage, this.node).then(result => this.launchConfirmationModal(result));
    this.close();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.mileage);
  }


}
