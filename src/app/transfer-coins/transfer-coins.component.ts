import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { TransferOrder } from './../transferOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-transfer-coins',
  templateUrl: './transfer-coins.component.html',
  styleUrls: ['./transfer-coins.component.scss']
})
export class TransferCoinsComponent implements OnInit {

  node: number;
  cordaName: string;
  transferOrder = new TransferOrder();
  submitted = false;

  constructor(private issueService: IssueCurrencyService, private dialog: MatDialog, private dialogRef: MatDialogRef<TransferCoinsComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
   }

  ngOnInit() {
  }

  transferCurrency(): void{
    this.submitted = true;
    this.issueService.transferCurrency(this.transferOrder, this.node).then(result => this.launchConfirmationModal(result));
    this.save();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.transferOrder);
  }


}
