import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { Car } from './../car';
import { MatInputModule } from '@angular/material';
import { RegisterCarService } from './../register-car.service';
import { IssueCurrencyService } from './../issue-currency.service';
import { ConfimationComponent } from '../confimation/confimation.component';
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-register-car',
  templateUrl: './register-car.component.html',
  styleUrls: ['./register-car.component.scss']
})
export class RegisterCarComponent implements OnInit {

  submitted = false;
  car = new Car();
  node: number;

  ngOnInit() {
  }

  constructor(private registerCarService: RegisterCarService, private dialog: MatDialog, private dialogRef: MatDialogRef<RegisterCarComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
   }


  registerCar(): void {
    this.submitted = true;
    this.registerCarService.createCar(this.car, this.node).then(result => this.launchConfirmationModal(result));
    this.close();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }


  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.car);
  }


}
