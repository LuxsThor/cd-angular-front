import { TestBed } from '@angular/core/testing';

import { IssuerDataService } from './issuer-data.service';

describe('IssuerServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IssuerServiceService = TestBed.get(IssuerServiceService);
    expect(service).toBeTruthy();
  });
});
