import { Serializable } from './serializable';

export class Offer implements Serializable<Offer>{
  price: number;
  eventType: string;
  uploader: string;

  deserialize(input: any) {
    this.price = input.key;
    this.eventType = input.value.eventType;
    this.uploader = input.value.publisher;
    return this;
  }
}
