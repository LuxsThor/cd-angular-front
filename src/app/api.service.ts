import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {Car} from './car';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: Http
  ) { }
/*
  public getAllCars(): Observable<Car[]> {
  return this.http
    .get(API_URL + '/cars')
    .map(response => {
      const cars = response.json();
      return cars.map((car) => new Car(car));
    })
    .catch(this.handleError);
}

  public addCar(car: Car): Observable<Car> {
  return this.http
    .post(API_URL + '/cars', car)
    .map(response => {
      return new Car(response.json());
    })
    .catch(this.handleError);
  }

  public getCarById(carId: number): Observable<Car> {
  return this.http
    .get(API_URL + '/cars/' + carId)
    .map(response => {
      return new Car(response.json());
    })
    .catch(this.handleError);
}

  private handleError (error: Response | any) {
  console.error('ApiService::handleError', error);
  return Observable.throw(error);
}*/

}
