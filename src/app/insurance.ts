import { Serializable } from './serializable';

export class Insurance implements Serializable<Insurance>{
  carId: string;
  acceptor: string;
  description: string;

  deserialize(input: any) {
    this.carId = input.carId.id;
    this.description = input.description;
    return this;
  }
}
