import { TestBed } from '@angular/core/testing';

import { RegisterCarService } from './register-car.service';

describe('RegisterCarService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RegisterCarService = TestBed.get(RegisterCarService);
    expect(service).toBeTruthy();
  });
});
