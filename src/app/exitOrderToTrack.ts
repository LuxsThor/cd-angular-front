export class ExitOrderToTrack {
  amount: number;
  paidBy: string;

  deserialize(input: any) {
    this.amount = input.third.props.amount;
    this.paidBy = input.third.props.paidBy;
  }
}
