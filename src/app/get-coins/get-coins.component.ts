import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { IssueOrder } from './../issueOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-get-coins',
  templateUrl: './get-coins.component.html',
  styleUrls: ['./get-coins.component.scss']
})
export class GetCoinsComponent implements OnInit {

  node: number;
  cordaName: string;

  constructor(private issueService: IssueCurrencyService, private dialog: MatDialog, private dialogRef: MatDialogRef<GetCoinsComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
    switch(this.node) {
     case 10012: {
        this.cordaName = "O=Car-Owner, L=Zuerich, C=CH";
        this.issueOrder.receiver = this.cordaName;
        break;
     }
     case 10015: {
        this.cordaName = "O=GarageX, L=Zuerich, C=CH";
        this.issueOrder.receiver = this.cordaName;
        break;
     }
     case 10018: {
        this.cordaName = "O=Axa, L=Zuerich, C=CH";
        this.issueOrder.receiver = this.cordaName;
        break;
     }
     case 10021: {
        this.cordaName = "O=CD-Buyer, L=Zuerich, C=CH";
        this.issueOrder.receiver = this.cordaName;
        break;
     }
     default: {
        this.cordaName = "not set";
        break;
     }
    }
   }

  issueOrder = new IssueOrder();
  submitted = false;

  ngOnInit() {
  }

  issueCurrency(): void{
    this.submitted = true;
    this.issueService.createCurrency(this.issueOrder, 10009).then(result => this.launchConfirmationModal(result));
    this.save();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.issueOrder);
  }


}
