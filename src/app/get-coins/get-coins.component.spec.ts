import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GetCoinsComponent } from './get-coins.component';

describe('GetCoinsComponent', () => {
  let component: GetCoinsComponent;
  let fixture: ComponentFixture<GetCoinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GetCoinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GetCoinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
