import { Serializable } from './serializable';

export class InsuranceUpload implements Serializable<InsuranceUpload>{
  toCar: string;
  eventId: string;
  uploader: string;
  description: number;

  deserialize(input: any) {
    this.toCar = input.state.data.carId.id;
    this.eventId = input.state.data.linearId.id;
    this.uploader = input.state.data.uploader;
    this.description = input.state.data.description;
    return this;
  }
}
