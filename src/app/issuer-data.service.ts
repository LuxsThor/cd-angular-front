import { Injectable } from '@angular/core';
import { ALLORDERS } from './issueOrders';
import { ALLEXITS } from './allExits';
import { BALANCE } from './balance';
import { IssueOrder } from './issueOrder';
import { ExitOrder } from './exitOrder';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IssuerDataService {

  constructor() { }



  getIssueOrders(): IssueOrder[] {
    return ALLORDERS;
  }

  getExitOrders(): ExitOrder[] {
    return ALLEXITS;
  }

  getBalance(): number {
    let balance = 0;
    for (let issued of ALLORDERS) {
    balance += issued.amount;
    }
    for (let exited of ALLEXITS) {
    balance -= exited.amount;
    }
    return balance;
  }

}
