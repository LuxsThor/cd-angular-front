import { Serializable } from './serializable';

export class DataUpload implements Serializable<DataUpload>{
  type: number;
  toCar: number;
  eventId: string;

  deserialize(input: any) {
    this.type = input.mileagePrice;
    this.toCar = input.insurancePrice;
    this.eventId = input.insurancePrice;
    return this;
  }
}
