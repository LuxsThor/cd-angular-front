import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ng2OdometerModule } from 'ng2-odometer';
import { IssueCurrencyComponent } from '../issue-currency/issue-currency.component';
import { TransferCoinsComponent } from '../transfer-coins/transfer-coins.component';
import { UploadInsuranceComponent } from '../upload-insurance/upload-insurance.component';
import { UploadMileageComponent } from '../upload-mileage/upload-mileage.component';
import { UploadServiceComponent } from '../upload-service/upload-service.component';
import { UploadPricingComponent } from '../upload-pricing/upload-pricing.component';
import { SellCoinsComponent } from '../sell-coins/sell-coins.component';
import { BuyDataComponent } from '../buy-data/buy-data.component';
import { RegisterCarComponent } from '../register-car/register-car.component';
import { GetCoinsComponent } from '../get-coins/get-coins.component';
import { PriceOfferComponent } from '../price-offer/price-offer.component';

import { MatDialogModule } from "@angular/material";
import { MatMenuModule } from '@angular/material/menu';
import { PriceUpload } from '../priceUpload';
import { ServiceUpload } from './../serviceUpload';
import { InsuranceUpload } from './../insuranceUpload';
import { MatTableModule } from '@angular/material/table';
import { NodeDataService } from './../node-data.service';
import { MyCar } from './../myCar';
import { ExitOrderToTrack } from './../exitOrderToTrack';
import { MileageUpload } from './../mileageUpload';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from "@angular/material";
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";
import { ALLORDERS } from './../issueOrders';
import { ALLEXITS } from './../allExits';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  node: number;
  nodeName: string;
  myprices: PriceUpload;
  balance: number;
  myCars: MyCar[] = []
  mileageData: MileageUpload[] = []
  mileageDataBought: MileageUpload[] = []
  serviceData: ServiceUpload[] = []
  serviceDataBought: ServiceUpload[] = []
  insuranceData: InsuranceUpload[] = []
  insuranceDataBought: InsuranceUpload[] = []

  constructor(private router: Router, private dialog: MatDialog, private dataService: NodeDataService, private dialogRef: MatDialogRef<DashboardComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
  }

  ngOnInit(): void {
    this.update();
    this.assignNodeName();
  }

  update() {
    this.getMyCars();
    this.getMyMileage();
    this.getMyBoughtMileage();
    this.getMyService();
    this.getMyBoughtService();
    this.getMyInsurance();
    this.getMyBoughtInsurance();
    this.getMyPrices();
    this.getMyBalance();
  }

  checkState() {

  }

  assignNodeName(){
    switch(this.node) {
     case 10009: {
        this.nodeName = "Bank of Cardossier"
        break;
     }
     case 10012: {
        this.nodeName = "Fahrzeugbesitzer"
        break;
     }
     case 10015: {
        this.nodeName = "GarageX"
        break;
     }
     case 10018: {
        this.nodeName = "Axa"
        break;
     }
     case 10021: {
        this.nodeName = "Käufer"
        break;
     }
     default: {
        this.nodeName = "Unassigned Port"
        break;
     }
    }
  }

  launchInsuranceUpload(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(UploadInsuranceComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchMileageUpload(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(UploadMileageComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchBuyData(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(BuyDataComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchRequestOffer(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(PriceOfferComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchGetCurrency(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(GetCoinsComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
      data => ALLORDERS.push(data)
    );
  }

  launchTransferCurrency(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(TransferCoinsComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchExitCurrency(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(SellCoinsComponent, dialogConfig);


    dialogRef.afterClosed().subscribe(
      data => ALLEXITS.push(data)
    );
  }




  launchServiceUpload(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(UploadServiceComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchSetPricing(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(UploadPricingComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from setpricing modal:", data),
    );
  }

  launchCarRegistration(){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: this.node
      };
    const dialogRef = this.dialog.open(RegisterCarComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  getMyPrices() {
    this.dataService.getPrices(this.node).then(
      res => this.myprices = res,
    );
  }

  getMyBalance() {
    this.dataService.getBalance(this.node).then(
      balance => this.balance = balance,
    );
  }

  getMyCars() {
    this.dataService.getMyCars(this.node).then(
      cars => this.myCars = cars,
    );
  }

  getMyMileage() {
    this.dataService.getMyMileage(this.node).then(
      mileageEvents => this.mileageData = mileageEvents,
    );
  }

  getMyBoughtMileage() {
    this.dataService.getMyBoughtMileage(this.node).then(
      mileageEvents => this.mileageDataBought = mileageEvents,
    );
  }

  getMyService() {
    this.dataService.getMyService(this.node).then(
      serviceEvents => this.serviceData = serviceEvents,
    );
  }

  getMyBoughtService() {
    this.dataService.getMyBoughtService(this.node).then(
      serviceEvents => this.serviceDataBought = serviceEvents,
    );
  }

  getMyInsurance() {
    this.dataService.getMyInsurance(this.node).then(
      insuranceEvents => this.insuranceData = insuranceEvents,
    );
  }

  getMyBoughtInsurance() {
    this.dataService.getMyBoughtInsurance(this.node).then(
      insuranceEvents => this.insuranceDataBought = insuranceEvents,
    );
  }



  close() {
    this.dialogRef.close();
  }

  save() {
    this.checkState();
    this.dialogRef.close(this.myprices);
  }


}
