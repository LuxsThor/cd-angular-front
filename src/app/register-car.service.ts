import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { Car } from './car';
import { MatInputModule } from '@angular/material';
import { Tx } from './tx';

@Injectable({
  providedIn: 'root'
})
export class RegisterCarService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

    createCar(car: Car, node:number): Promise<string> {
      let url = this.getUrl('/api/cd-demo/create-car', node);
      console.log(url);
      console.log(JSON.stringify(car));
      return this.http
        .put(url, JSON.stringify(car), { headers: this.headers })
        .toPromise()
        .then(
          res => new Tx().deserialize(res).txResponse);
    }

    getUrl(path: string, node: number) {
      return this.urlService.url + ':' + node + path;
    }

}
