import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { IssueOrder } from './../issueOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { IssueCurrencyComponent } from '../issue-currency/issue-currency.component';

@Component({
  selector: 'app-confimation',
  templateUrl: './confimation.component.html',
  styleUrls: ['./confimation.component.scss']
})
export class ConfimationComponent implements OnInit {

  confirmationmsg: string;

  constructor(@Inject(MAT_DIALOG_DATA) data, private dialog: MatDialog, private dialogRef: MatDialogRef<ConfimationComponent>) {
    this.confirmationmsg = data.confirmationmsg;
  }

  ngOnInit() {
  }

  save() {
        this.dialogRef.close(this.confirmationmsg);
    }

  close() {
     this.dialogRef.close();
  }

}
