import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { ExitOrder } from './../exitOrder';
import { TransferOrder } from './../transferOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";
import { ExitOrderToTrack } from './../exitOrderToTrack';

@Component({
  selector: 'app-sell-coins',
  templateUrl: './sell-coins.component.html',
  styleUrls: ['./sell-coins.component.scss']
})
export class SellCoinsComponent implements OnInit {

  node: number;
  cordaName: string;

  constructor(private issueService: IssueCurrencyService, private dialog: MatDialog, private dialogRef: MatDialogRef<SellCoinsComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
   }

  transferOrder = new TransferOrder();
  exitOrderToSend = new ExitOrder();
  exitOrderToTrack = new ExitOrderToTrack;
  submitted = false;

  ngOnInit() {
    this.transferOrder.receiver = "O=Cardossier, L=Zuerich, C=CH";
    this.assignNodeName();
  }

  assignNodeName(){
    switch(this.node) {
     case 10009: {
        this.cordaName = "Bank of Cardossier"
        break;
     }
     case 10012: {
        this.cordaName = "Car Owner"
        break;
     }
     case 10015: {
        this.cordaName = "GarageX"
        break;
     }
     case 10018: {
        this.cordaName = "Axa"
        break;
     }
     case 10021: {
        this.cordaName = "CD Buyer"
        break;
     }
     default: {
        this.cordaName = "Unassigned Port"
        break;
     }
    }
  }

  exitCurrency(): void{
    this.submitted = true;
    this.transferOrder.amount = this.exitOrderToSend.amount;
    this.exitOrderToTrack.amount = this.exitOrderToSend.amount;
    this.exitOrderToTrack.paidBy = this.cordaName;
    this.issueService.transferCurrency(this.transferOrder, this.node).then(result => this.launchConfirmationModal(result)).then(result => this.exitOffBankOfCD());
    this.save();
  }

  exitOffBankOfCD(): void{
    this.issueService.exitCurrency(this.exitOrderToSend, 10009).then(result => this.launchConfirmationModal(result));
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.exitOrderToTrack);
  }

}
