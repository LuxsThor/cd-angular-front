import { Injectable } from '@angular/core';
import { IssueOrder } from './issueOrder';
import { BuyDataOrder } from './buyDataOrder';
import { ExitOrder } from './exitOrder';
import { PriceUpload } from './priceUpload';
import { Observable, of } from 'rxjs';
import { UrlService } from './url.service';
import { PortService } from './port.service';
import { Headers, Http } from '@angular/http';
import { MyCar } from './myCar';
import { MileageUpload } from './mileageUpload';
import { ServiceUpload } from './serviceUpload';
import { InsuranceUpload } from './insuranceUpload';
import { Tx } from './tx';


@Injectable({
  providedIn: 'root'
})


export class NodeDataService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

    getObject = jsonResult => {
      console.log("result object value: ", jsonResult);
    }

  getPrices(node: number): Promise<PriceUpload> {
    let url = this.getUrl('/api/cd-demo/my-prices', node);
    console.log(url);
    return this.http.get(url)
      .toPromise()
      .then(
         response => new PriceUpload().deserialize(response.json()[0].state.data) as PriceUpload
        //response => this.getObject(response.json())
        // response => new PriceUpload().deserialize(response.json()) as PriceUpload
      );
  }

  getBalance(node: number): Promise<number> {
    let url = this.getUrl('/api/cd-demo/balance', node);
    console.log(url);
    return this.http.get(url)
      .toPromise()
      .then(
        response => response.json() as number,
      );
  }

  getMyCars(node: number): Promise<MyCar[]> {
    let url = this.getUrl('/api/cd-demo/cars', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createCarsArray(res.json()) as MyCar[],
      );
  }

  getMyMileage(node: number): Promise<MileageUpload[]> {
    let url = this.getUrl('/api/cd-demo/mileage-event', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createMileageArray(res.json()) as MileageUpload[],
      );
  }

  getMyBoughtMileage(node: number): Promise<MileageUpload[]> {
    let url = this.getUrl('/api/cd-demo/my-mileage-purchases', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createMileageArray(res.json()) as MileageUpload[],
      );
  }

  getMyService(node: number): Promise<ServiceUpload[]> {
    let url = this.getUrl('/api/cd-demo/service-events', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createServiceArray(res.json()) as ServiceUpload[],
      );
  }

  getMyBoughtService(node: number): Promise<ServiceUpload[]> {
    let url = this.getUrl('/api/cd-demo/my-service-purchases', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createServiceArray(res.json()) as ServiceUpload[],
      );
  }

  getMyInsurance(node: number): Promise<InsuranceUpload[]> {
    let url = this.getUrl('/api/cd-demo/insurance-events', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createInsuranceArray(res.json()) as InsuranceUpload[],
      );
  }

  getMyBoughtInsurance(node: number): Promise<InsuranceUpload[]> {
    let url = this.getUrl('/api/cd-demo/my-insurance-purchases', node);
    return this.http.get(url)
      .toPromise()
      .then(
        res => this.createInsuranceArray(res.json()) as InsuranceUpload[],
      );
  }


  private createCarsArray(input: any): MyCar[] {
    let cars = new Array<MyCar>();
    input.forEach((element: string[]) => {
      let receivedCar = new MyCar().deserialize(element);
      cars.push(receivedCar);
    });
    return cars;
  }

  private createMileageArray(input: any): MileageUpload[] {
    let mileageEvents = new Array<MileageUpload>();
    input.forEach((element: string[]) => {
      let receivedEvent = new MileageUpload().deserialize(element);
      mileageEvents.push(receivedEvent);
    });
    return mileageEvents;
  }

  private createServiceArray(input: any): ServiceUpload[] {
    let serviceEvents = new Array<ServiceUpload>();
    input.forEach((element: string[]) => {
      let receivedEvent = new ServiceUpload().deserialize(element);
      serviceEvents.push(receivedEvent);
    });
    return serviceEvents;
  }

  private createInsuranceArray(input: any): InsuranceUpload[] {
    let insuranceEvents = new Array<InsuranceUpload>();
    input.forEach((element: string[]) => {
      let receivedEvent = new InsuranceUpload().deserialize(element);
      insuranceEvents.push(receivedEvent);
    });
    return insuranceEvents;
  }

  createBuyOrder(order: BuyDataOrder, node:number): Promise<string> {
    let url = this.getUrl('/api/cd-demo/buy-data', node);
    console.log("this is the url to send to"+ url);
    console.log(JSON.stringify(order));
    return this.http
      .put(url, JSON.stringify(order), { headers: this.headers })
      .toPromise()
      .then(
        res => new Tx().deserialize(res).txResponse);
  }



  /*
  getUploads(): IssueOrder[] {
    return ALLORDERS;
  }

  getPurchases(): ExitOrder[] {
    return ALLEXITS;
  }
  */
  getCars(){

  }
  /*
  getCoins(): number {
    let balance = 0;
    for (let issued of ALLORDERS) {
    balance += issued.amount;
    }
    for (let exited of ALLEXITS) {
    balance -= exited.amount;
    }
    return balance;
  }
  */
  getUrl(path: string, node: number) {
    return this.urlService.url + ':' + node + path;
  }

}
