import { Injectable } from '@angular/core';

@Injectable()
export class PortService {

  public current: number;
  public nodes: Array<Number> = [10009, 10012, 10015];

  constructor() {
    if (location.port !== '4200') {
      let port = Number(location.port);
      this.current = port;
    } else {
      switch (location.pathname) {
        case '/dashboard-issuer':
          this.current = 10009;
          break;
        case '/dashboard-amag':
          this.current = 10012;
          break;
        case '/dashboard-garage':
          this.current = 10015;
          break;
      }
    }
  }
}
