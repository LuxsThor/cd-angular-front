import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogModule } from "@angular/material";
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import * as $ from 'jquery';
import { IssueCurrencyComponent } from '../issue-currency/issue-currency.component';
import { UploadInsuranceComponent } from '../upload-insurance/upload-insurance.component';
import { UploadMileageComponent } from '../upload-mileage/upload-mileage.component';
import { UploadServiceComponent } from '../upload-service/upload-service.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { IssuerDashboardComponent } from '../issuer-dashboard/issuer-dashboard.component';
import { RegisterCarComponent } from '../register-car/register-car.component';
import { MatDialogConfig} from "@angular/material";


@Component({
  selector: 'app-map-home',
  templateUrl: './map-home.component.html',
  styleUrls: ['./map-home.component.scss']
})
export class MapHomeComponent implements OnInit {
  expandTxt = '>';
  shrinkTxt = '<';
  buttonTxt = this.expandTxt;
  unfolded = false;

  constructor(private router: Router, private dialog: MatDialog) { }

  ngOnInit() {
  }

  expandMenu() {
    $(this).toggleClass('change');
    $('.sidenav').toggleClass('sidenav-toggle');
    $('.container').toggleClass('container-toggle');
    if (!this.unfolded) {
      $('#myList').removeClass('rolldown-list');
      setTimeout(function () {
        $('#myList').addClass('rolldown-list');
      }, 1);
      this.unfolded = true;
    }
    if (this.buttonTxt === this.expandTxt) {
      this.buttonTxt = this.shrinkTxt;
    } else {
      this.buttonTxt = this.expandTxt;
    }
  }

  launchCarRegistration(){
    const dialogRef = this.dialog.open(RegisterCarComponent, { width: '85%', height: '85%' });

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchIssueCurrency(){

    const dialogRef = this.dialog.open(IssueCurrencyComponent, { width: '85%', height: '85%' });

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchInsuranceUpload(){
    const dialogRef = this.dialog.open(UploadInsuranceComponent, { width: '85%', height: '85%' });

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchMileageUpload(){
    const dialogRef = this.dialog.open(UploadMileageComponent, { width: '85%', height: '85%' });

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchServiceUpload(){
    const dialogRef = this.dialog.open(UploadServiceComponent, { width: '85%', height: '85%' });

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchDashboard(nodePort: number){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: nodePort
      };
    const dialogRef = this.dialog.open(DashboardComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }

  launchIssuerDashboard(port: number){

    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    const dialogRef = this.dialog.open(IssuerDashboardComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output:", data)
    );
  }


}
