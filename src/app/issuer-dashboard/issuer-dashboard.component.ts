import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IssueCurrencyComponent } from '../issue-currency/issue-currency.component';
import { MatDialogModule } from "@angular/material";
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { NodeDataService } from './../node-data.service';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import { MatDialogConfig} from "@angular/material";
import { IssueOrder } from './../issueOrder';
import { Ng2OdometerModule } from 'ng2-odometer';
import { ExitOrder } from './../exitOrder';
import { IssueCurrencyService } from './../issue-currency.service';
import { IssuerDataService } from './../issuer-data.service';
import { ExitCurrencyComponent } from '../exit-currency/exit-currency.component';
import { ALLORDERS } from './../issueOrders';
import { ALLEXITS } from './../allExits';
import { BALANCE } from './../balance';

@Component({
  selector: 'app-issuer-dashboard',
  templateUrl: './issuer-dashboard.component.html',
  styleUrls: ['./issuer-dashboard.component.scss']
})
export class IssuerDashboardComponent implements OnInit {
  node = 10009;
  issueOrders: IssueOrder[];
  exitOrders: ExitOrder[];
  currentBalance: number;
  balanceOwning: number;

  constructor(private router: Router, private dialog: MatDialog, private dataService: NodeDataService, issueService: IssueCurrencyService, private dialogRef: MatDialogRef<IssuerDashboardComponent>, private issuerDataService: IssuerDataService) { }

  ngOnInit() {
    this.update();
  }

  update() {
    this.getOrders();
    this.getExits();
    this.getMyBalance();
    this.getBalance();
  }

  ngOnChanges(){

  }
  getMyBalance() {
    this.dataService.getBalance(this.node).then(
      balance => this.balanceOwning = balance,
    );
  }

  getOrders(): void {
    this.issueOrders = this.issuerDataService.getIssueOrders();
  }

  getExits(): void {
    this.exitOrders = this.issuerDataService.getExitOrders();
  }

  getBalance(): void {
    this.currentBalance = this.issuerDataService.getBalance();
  }

  launchIssueCurrency(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: 10009
      };

    const dialogRef = this.dialog.open(IssueCurrencyComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => ALLORDERS.push(data)
    );
  }

  launchExitCurrency(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '85%';
    dialogConfig.height = '85%';
    dialogConfig.data = {
          node: 10009
      };

    const dialogRef = this.dialog.open(ExitCurrencyComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
      data => ALLEXITS.push(data)
    );
    this.getBalance();
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.issueOrders);
  }

  /*
  update() {
    this.issueService.getPurchaseOrders().then(purchaseOrders => this.purchaseOrders = purchaseOrders);
  }*/

}
