export class BuyDataOrder {
  carId: string;
  eventType: string;
  acceptableMileagePrice: number;
  acceptableInsurancePrice: number;
  acceptableServicePrice: number;

  deserialize(input: any) {
    this.carId = input.state.data.carId;
    this.eventType = input.state.data.eventType;
    this.acceptableMileagePrice = input.state.data.acceptableMileagePrice;
    this.acceptableInsurancePrice = input.state.data.acceptableInsurancePrice;
    this.acceptableServicePrice = input.state.data.acceptableServicePrice;
  }
}
