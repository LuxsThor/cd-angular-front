import { TestBed } from '@angular/core/testing';

import { RequestCdService } from './request-cd.service';

describe('RequestCdService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestCdService = TestBed.get(RequestCdService);
    expect(service).toBeTruthy();
  });
});
