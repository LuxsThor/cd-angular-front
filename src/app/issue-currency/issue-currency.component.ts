import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { IssueOrder } from './../issueOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";



@Component({
  selector: 'app-issue-currency',
  templateUrl: './issue-currency.component.html',
  styleUrls: ['./issue-currency.component.scss']
})
export class IssueCurrencyComponent implements OnInit {

  node: number;

  constructor(private issueService: IssueCurrencyService, private dialog: MatDialog, private dialogRef: MatDialogRef<IssueCurrencyComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
   }

  issueOrder = new IssueOrder();
  submitted = false;

  ngOnInit() {
  }

  issueCurrency(): void{
    this.submitted = true;
    this.issueService.createCurrency(this.issueOrder, this.node).then(result => this.launchConfirmationModal(result));
    this.save();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.issueOrder);
  }

}
