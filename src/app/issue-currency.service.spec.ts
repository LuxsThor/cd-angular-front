import { TestBed } from '@angular/core/testing';

import { IssueCurrencyService } from './issue-currency.service';

describe('IssueCurrencyService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IssueCurrencyService = TestBed.get(IssueCurrencyService);
    expect(service).toBeTruthy();
  });
});
