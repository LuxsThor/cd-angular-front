import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { BuyDataOrder } from './../buyDataOrder';
import { MatInputModule } from '@angular/material';
import { NodeDataService } from './../node-data.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';


@Component({
  selector: 'app-buy-data',
  templateUrl: './buy-data.component.html',
  styleUrls: ['./buy-data.component.scss']
})
export class BuyDataComponent implements OnInit {

  buyOrder = new BuyDataOrder();
  eventType: string = "";
  submitted = false;
  node: number;

  constructor(private dataService: NodeDataService, private dialog: MatDialog, private dialogRef: MatDialogRef<BuyDataComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
   }

  ngOnInit() {
  }

  buyData(){
    this.submitted = true;
    this.dataService.createBuyOrder(this.buyOrder, this.node).then(result => this.launchConfirmationModal(result));
    this.close();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.buyOrder);
  }
}
