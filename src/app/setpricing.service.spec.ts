import { TestBed } from '@angular/core/testing';

import { SetpricingService } from './setpricing.service';

describe('SetpricingService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SetpricingService = TestBed.get(SetpricingService);
    expect(service).toBeTruthy();
  });
});
