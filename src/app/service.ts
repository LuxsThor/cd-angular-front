import { Serializable } from './serializable';

export class Service implements Serializable<Service>{
  carId: string;
  acceptor: string;
  description: number;

  deserialize(input: any) {
    this.carId = input.carId.id;
    this.description = input.description;
    return this;
  }
}
