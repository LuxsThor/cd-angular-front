import { Serializable } from './serializable';

export class Mileage implements Serializable<Mileage>{
  carId: string;
  km: number;

  deserialize(input: any) {
    this.carId = input.carId.id;
    this.km = input.km;
    return this;
  }
}
