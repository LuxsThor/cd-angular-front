import { TestBed } from '@angular/core/testing';

import { UploadMileageService } from './upload-mileage.service';

describe('UploadMileageService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadMileageService = TestBed.get(UploadMileageService);
    expect(service).toBeTruthy();
  });
});
