import { Serializable } from './serializable';

export class PriceUpload implements Serializable<PriceUpload>{
  mileagePrice: number;
  insurancePrice: number;
  servicePrice: number;

  deserialize(input: any) {
    this.mileagePrice = input.mileagePrice;
    this.insurancePrice = input.insurancePrice;
    this.servicePrice = input.servicePrice;
    return this;
  }
}
