import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { PriceUpload } from './priceUpload';
import { MatInputModule } from '@angular/material';
import { Tx } from './tx';

@Injectable({
  providedIn: 'root'
})
export class SetpricingService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

    createPricing(price: PriceUpload, node:number): Promise<string> {
      let url = this.getUrl('/api/cd-demo/set-pricing', node);
      console.log(url);
      console.log(JSON.stringify(price));
      return this.http
        .put(url, JSON.stringify(price), { headers: this.headers })
        .toPromise()
        .then(
          res => new Tx().deserialize(res).txResponse);
    }
  

    getUrl(path: string, node: number) {
      return this.urlService.url + ':' + node + path;
    }

}
