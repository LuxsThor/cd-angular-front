import { Serializable } from './serializable';

export class Car implements Serializable<Car>{
  make: string;
  model: string;
  licenseplate: string;

  deserialize(input: any) {
    this.make = input.third.props.make;
    this.model = input.third.props.model;
    this.licenseplate = input.third.props.licenseplate;
    return this;
  }
}
