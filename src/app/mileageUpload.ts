import { Serializable } from './serializable';

export class MileageUpload implements Serializable<MileageUpload>{
  toCar: string;
  eventId: string;
  uploader: string;
  km: number;

  deserialize(input: any) {
    this.toCar = input.state.data.carId.id;
    this.eventId = input.state.data.linearId.id;
    this.uploader = input.state.data.uploader;
    this.km = input.state.data.value;
    return this;
  }
}
