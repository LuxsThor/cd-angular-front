import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatToolbarModule } from '@angular/material';
import { MatDialogModule } from "@angular/material";
import { MatDialog } from '@angular/material';
import { MatDialogRef } from '@angular/material';
import { FormsModule }   from '@angular/forms';
import { MatInputModule } from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { HttpModule } from '@angular/http';
import {MatRadioModule} from '@angular/material/radio';
import { RegisterCarService } from './register-car.service';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { Ng2OdometerModule } from 'ng2-odometer';
import { SetpricingService } from './setpricing.service';
import { IssuerDataService } from './issuer-data.service';
import { NodeDataService } from './node-data.service';
import { DataRequestService } from './request-cd.service';


import * as $ from 'jquery'

import { AppComponent } from './app.component';
import { MapHomeComponent } from './map-home/map-home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { IssueCurrencyComponent } from './issue-currency/issue-currency.component';
import { UploadMileageComponent } from './upload-mileage/upload-mileage.component';
import { UploadServiceComponent } from './upload-service/upload-service.component';
import { UploadInsuranceComponent } from './upload-insurance/upload-insurance.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UploadPricingComponent } from './upload-pricing/upload-pricing.component';
import { RegisterCarComponent } from './register-car/register-car.component';
import { IssuerDashboardComponent } from './issuer-dashboard/issuer-dashboard.component';
import { ConfimationComponent } from './confimation/confimation.component';
import { ExitCurrencyComponent } from './exit-currency/exit-currency.component';
import {NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeNestedDataSource} from '@angular/material/tree';
import { GetCoinsComponent } from './get-coins/get-coins.component';
import { SellCoinsComponent } from './sell-coins/sell-coins.component';
import { BuyDataComponent } from './buy-data/buy-data.component';
import { TransferCoinsComponent } from './transfer-coins/transfer-coins.component';
import { PriceOfferComponent } from './price-offer/price-offer.component';
import { OfferModalComponent } from './offer-modal/offer-modal.component';



const routes: Routes = [
  { path: 'home', component: MapHomeComponent},
  { path: 'create-car', component: RegisterCarComponent },
  { path: 'issue', component: IssueCurrencyComponent },
  { path: 'exit', component: ExitCurrencyComponent },
  { path: 'insurance', component: UploadInsuranceComponent },
  { path: 'mileage', component: UploadMileageComponent },
  { path: 'service', component: UploadServiceComponent },
  { path: 'pricing', component: UploadServiceComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'confirmation', component: ConfimationComponent },
  { path: 'offer', component: PriceOfferComponent },
  { path: 'buydata', component: BuyDataComponent },
  { path: 'getcoins', component: GetCoinsComponent },
  { path: 'transfer', component: TransferCoinsComponent },
  { path: 'getoffers', component: PriceOfferComponent },
  { path: 'sellcoins', component: SellCoinsComponent },
  { path: 'issuer-dashboard', component: IssuerDashboardComponent },
  { path: '', redirectTo: 'home', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    MapHomeComponent,
    IssueCurrencyComponent,
    UploadMileageComponent,
    UploadServiceComponent,
    UploadInsuranceComponent,
    DashboardComponent,
    UploadPricingComponent,
    RegisterCarComponent,
    IssuerDashboardComponent,
    ConfimationComponent,
    ExitCurrencyComponent,
    GetCoinsComponent,
    SellCoinsComponent,
    BuyDataComponent,
    TransferCoinsComponent,
    PriceOfferComponent,
    OfferModalComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(routes),
    MatToolbarModule,
    MatDialogModule,
    FormsModule,
    HttpModule,
    MatInputModule,
    MatMenuModule,
    MatRadioModule,
    Ng2OdometerModule.forRoot()
  ],
  providers: [RegisterCarService, PortService, UrlService, SetpricingService, IssuerDataService, NodeDataService, DataRequestService],
  entryComponents: [IssueCurrencyComponent, UploadMileageComponent, UploadPricingComponent, RegisterCarComponent, ExitCurrencyComponent, PriceOfferComponent, OfferModalComponent],
  bootstrap: [AppComponent]

})
export class AppModule { }
