import { Serializable } from './serializable';

export class ServiceUpload implements Serializable<ServiceUpload>{
  toCar: string;
  eventId: string;
  uploader: string;
  description: string;

  deserialize(input: any) {
    this.toCar = input.state.data.carId.id;
    this.eventId = input.state.data.linearId.id;
    this.uploader = input.state.data.uploader;
    this.description = input.state.data.description;
    return this;
  }
}
