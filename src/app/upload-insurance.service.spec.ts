import { TestBed } from '@angular/core/testing';

import { UploadInsuranceService } from './upload-insurance.service';

describe('UploadInsuranceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadInsuranceService = TestBed.get(UploadInsuranceService);
    expect(service).toBeTruthy();
  });
});
