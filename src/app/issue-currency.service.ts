import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { IssueOrder } from './issueOrder';
import { ExitOrder } from './exitOrder';
import { TransferOrder } from './transferOrder';
import { MatInputModule } from '@angular/material';
import { Tx } from './tx';

@Injectable({
  providedIn: 'root'
})
export class IssueCurrencyService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

    createCurrency(order: IssueOrder, issuerNode: number): Promise<string> {
      let url = this.getUrl('/api/cd-demo/issue-currency', issuerNode);
      console.log(url);
      console.log(JSON.stringify(order));
      return this.http
        .put(url, JSON.stringify(order), { headers: this.headers })
        .toPromise()
        .then(
          res => new Tx().deserialize(res).txResponse);
    }

    exitCurrency(order: ExitOrder, issuerNode: number): Promise<string> {
      let url = this.getUrl('/api/cd-demo/exit-currency', issuerNode);
      console.log(location.pathname);
      console.log(url);
      console.log(JSON.stringify(order));
      return this.http
        .put(url, JSON.stringify(order), { headers: this.headers })
        .toPromise()
        .then(
          res => new Tx().deserialize(res).txResponse);
    }

    transferCurrency(order: TransferOrder, senderNode: number): Promise<string> {
      let url = this.getUrl('/api/cd-demo/transfer-currency', senderNode);
      console.log(location.pathname);
      console.log(url);
      console.log(JSON.stringify(order));
      return this.http
        .put(url, JSON.stringify(order), { headers: this.headers })
        .toPromise()
        .then(
          res => new Tx().deserialize(res).txResponse);
    }


    getUrl(path: string, node: number) {
      return this.urlService.url + ':' + node + path;
    }

    /*
    getIssueOrders(): Promise<IssueOrder[]> {
    let url = this.getUrl('/api/cd-demo/');
    return this.http.get(url)
      .toPromise()
      .then(
        response => this.createPurchaseOrderArray(response.json()) as PurchaseOrder[],
        err => this.handleError(err)
      );*/
  }
