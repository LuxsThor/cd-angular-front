export class ExitOrder {
  amount: number;

  deserialize(input: any) {
    this.amount = input.third.props.amount;
  }
}
