import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { DataRequest } from './../dataRequest';
import { Offer } from './../offer';
import { MatInputModule } from '@angular/material';
import { ConfimationComponent } from '../confimation/confimation.component';
import { OfferModalComponent } from '../offer-modal/offer-modal.component';
import { DataRequestService } from './../request-cd.service';
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-price-offer',
  templateUrl: './price-offer.component.html',
  styleUrls: ['./price-offer.component.scss']
})
export class PriceOfferComponent implements OnInit {

  dataRequest = new DataRequest();
  submitted = false;
  node: number;


  constructor(private dataRequestService: DataRequestService, private dialog: MatDialog, private dialogRef: MatDialogRef<PriceOfferComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
  }


  ngOnInit() {
  }

  makeRequest() {
    this.submitted = true;
    this.dataRequestService.makeRequest(this.dataRequest, this.node).then(result => this.launchOfferModal(result));
    this.close();
  }

  launchOfferModal(offers: Offer[]){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        offersList: offers
    };
    const dialogRef = this.dialog.open(OfferModalComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.dataRequest);
  }

}
