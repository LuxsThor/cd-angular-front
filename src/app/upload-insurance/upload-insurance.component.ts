import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { Insurance } from './../insurance';
import { MatInputModule } from '@angular/material';
import { ConfimationComponent } from '../confimation/confimation.component';
import { UploadInsuranceService } from './../upload-insurance.service';
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-upload-insurance',
  templateUrl: './upload-insurance.component.html',
  styleUrls: ['./upload-insurance.component.scss']
})
export class UploadInsuranceComponent implements OnInit {
  insuranceUpload = new Insurance();
  submitted = false;
  node: number;


  constructor(private uploadInsuranceService: UploadInsuranceService, private dialog: MatDialog, private dialogRef: MatDialogRef<UploadInsuranceComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
  }


  ngOnInit() {
  }

  uploadInsurance(){
    this.submitted = true;
    this.uploadInsuranceService.createUpload(this.insuranceUpload, this.node).then(result => this.launchConfirmationModal(result));
    this.close();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.insuranceUpload);
  }

}
