import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadInsuranceComponent } from './upload-insurance.component';

describe('UploadInsuranceComponent', () => {
  let component: UploadInsuranceComponent;
  let fixture: ComponentFixture<UploadInsuranceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadInsuranceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadInsuranceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
