import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadPricingComponent } from './upload-pricing.component';

describe('UploadPricingComponent', () => {
  let component: UploadPricingComponent;
  let fixture: ComponentFixture<UploadPricingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadPricingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadPricingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
