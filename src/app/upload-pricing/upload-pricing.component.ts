import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { PriceUpload } from './../priceUpload';
import { MatInputModule } from '@angular/material';
import { SetpricingService } from './../setpricing.service';
import { IssueCurrencyService } from './../issue-currency.service';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";
import { Inject } from '@angular/core';

@Component({
  selector: 'app-upload-pricing',
  templateUrl: './upload-pricing.component.html',
  styleUrls: ['./upload-pricing.component.scss']
})

export class UploadPricingComponent implements OnInit {

  ngOnInit() {
  }

  priceUpload = new PriceUpload();
  node: number;

  constructor(private setPricingService: SetpricingService, private dialog: MatDialog,
    private dialogRef: MatDialogRef<UploadPricingComponent>, @Inject (MAT_DIALOG_DATA) data) {
    this.node = data.node
  }

  setPrice(): void {
    this.setPricingService.createPricing(this.priceUpload, this.node).then(result => this.launchConfirmationModal(result));
    console.log("following is the whole setPricing function call")
    console.log(this.setPricingService.createPricing(this.priceUpload, this.node));
    this.save();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }


  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.priceUpload);
  }

}
