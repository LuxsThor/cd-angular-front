export class TransferOrder {
  amount: number;
  receiver: string;

  deserialize(input: any) {
    this.amount = input.third.props.amount;
    this.receiver = input.third.props.receiver;
  }
}
