import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExitCurrencyComponent } from './exit-currency.component';

describe('ExitCurrencyComponent', () => {
  let component: ExitCurrencyComponent;
  let fixture: ComponentFixture<ExitCurrencyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExitCurrencyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExitCurrencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
