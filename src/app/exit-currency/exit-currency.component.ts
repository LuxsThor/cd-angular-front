import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { ExitOrder } from './../exitOrder';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { ConfimationComponent } from '../confimation/confimation.component';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-exit-currency',
  templateUrl: './exit-currency.component.html',
  styleUrls: ['./exit-currency.component.scss']
})
export class ExitCurrencyComponent implements OnInit {

  exitOrder = new ExitOrder();
  submitted = false;
  node: number;

  constructor(private issueService: IssueCurrencyService, private dialog: MatDialog, private dialogRef: MatDialogRef<ExitCurrencyComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node;
   }

  ngOnInit() {
  }

  exitCurrency(): void{
    this.submitted = true;
    this.issueService.exitCurrency(this.exitOrder, this.node).then(result => this.launchConfirmationModal(result));
    console.log("following is the whole issuerservice create Currency function call")
    console.log(this.issueService.exitCurrency(this.exitOrder, this.node));
    this.save();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.exitOrder);
  }

}
