import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { Service } from './../service';
import { MatInputModule } from '@angular/material';
import { ConfimationComponent } from '../confimation/confimation.component';
import { UploadServiceService } from './../upload-service.service';
import { Inject } from '@angular/core';
import { MatDialogConfig} from "@angular/material";

@Component({
  selector: 'app-upload-service',
  templateUrl: './upload-service.component.html',
  styleUrls: ['./upload-service.component.scss']
})
export class UploadServiceComponent implements OnInit {
  serviceUpload = new Service();
  submitted = false;
  node: number;

  constructor(private uploadServiceService: UploadServiceService, private dialog: MatDialog, private dialogRef: MatDialogRef<UploadServiceComponent>, @Inject(MAT_DIALOG_DATA) data) {
    this.node = data.node
  }

  ngOnInit() {
  }

  uploadService(){
    this.submitted = true;
    this.uploadServiceService.createUpload(this.serviceUpload, this.node).then(result => this.launchConfirmationModal(result));
    this.close();
  }

  launchConfirmationModal(result:string){
    const dialogConfig = new MatDialogConfig();

    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.data = {
        confirmationmsg: result
    };
    const dialogRef = this.dialog.open(ConfimationComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(
        data => console.log("Dialog output from confirmation:", data)
    );
  }

  close() {
    this.dialogRef.close();
  }

  save() {
    this.dialogRef.close(this.serviceUpload);
  }

}
