import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { DataRequest } from './dataRequest';
import { Offer } from './offer';
import { MatInputModule } from '@angular/material';
import { Tx } from './tx';

@Injectable({
  providedIn: 'root'
})
export class DataRequestService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

  makeRequest(request: DataRequest, node:number): Promise<Offer[]> {
    let url = this.getUrl('/api/cd-demo/get-offer', node);
    console.log(url);
    console.log(JSON.stringify(request));
    return this.http
      .put(url, JSON.stringify(request), { headers: this.headers })
      .toPromise()
      .then(
        res => this.createOffersArray(res.json()) as Offer[]
    )}

  getUrl(path: string, node: number) {
    return this.urlService.url + ':' + node + path;
  }

  private createOffersArray(input: any): Offer[] {
    let offers = new Array<Offer>();
    input.forEach((element: string[]) => {
      let receivedOffer = new Offer().deserialize(element);
      offers.push(receivedOffer);
    });
    return offers;
  }
}
