import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';
import { PortService } from './port.service';
import { UrlService } from './url.service';
import { Service } from './service';
import { MatInputModule } from '@angular/material';
import { Tx } from './tx';

@Injectable({
  providedIn: 'root'
})
export class UploadServiceService {

  private headers = new Headers({ 'Content-Type': 'application/json' });

  constructor(private portService: PortService,
    private urlService: UrlService, private http: Http) { }

  createUpload(service: Service, node:number): Promise<string> {
    let url = this.getUrl('/api/cd-demo/publish-service', node);
    console.log(url);
    console.log(JSON.stringify(service));
    return this.http
      .put(url, JSON.stringify(service), { headers: this.headers })
      .toPromise()
      .then(
        res => new Tx().deserialize(res).txResponse);
  }

  getUrl(path: string, node: number) {
    return this.urlService.url + ':' + node + path;
  }
}
