import { Component } from '@angular/core';
import { Car } from './car';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'CD-Demo';

}
