import { Component, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef, MatDialog} from "@angular/material";
import { Offer } from './../offer';
import { MatInputModule } from '@angular/material';
import { IssueCurrencyService } from './../issue-currency.service';
import { Inject } from '@angular/core';
import { IssueCurrencyComponent } from '../issue-currency/issue-currency.component';

@Component({
  selector: 'app-offer-modal',
  templateUrl: './offer-modal.component.html',
  styleUrls: ['./offer-modal.component.scss']
})
export class OfferModalComponent implements OnInit {

  offers: Offer[];

  constructor(@Inject(MAT_DIALOG_DATA) data, private dialog: MatDialog, private dialogRef: MatDialogRef<OfferModalComponent>) {
    console.log("this is what the modal receives"+ data.offersList);
    this.offers = data.offersList;
  }

  ngOnInit() {
  }

  save() {
        this.dialogRef.close(this.offers);
    }

  close() {
     this.dialogRef.close();
  }

}
